import tkinter as tk
from tkinter import ttk
from tkinter import filedialog, StringVar, OptionMenu, font, messagebox, CENTER, NO
from tkinter.ttk import Button
import os
from PIL import Image


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.title("Lab2")
        self.option = Button(self, text="Choose a folder", 
            command=self.file_ch).grid(row=1, column=1)
        self.directory = None
        self.ok_main_btn = Button(self, text="OK", 
            command=self.print_info).grid(row=2, column=1)

    def file_ch(self):
        self.directory = filedialog.askdirectory()

    def print_info(self):
        if self.directory is None:
            return 0

        self.info_window = tk.Toplevel(self)
        self.info_window.geometry("800x300")
        self.im_table = ttk.Treeview(self.info_window)
        self.im_table.pack()

        self.im_table['columns']= ('name', 'size', 'dpi', 'depth', 'compression')
        self.im_table.column("#0", width=0, stretch=NO)
        self.im_table.column("name",anchor=CENTER, width=200)
        self.im_table.column("size", anchor=CENTER, width=100)
        self.im_table.column("dpi", anchor=CENTER, width=100)
        self.im_table.column("depth", anchor=CENTER, width=120)
        self.im_table.column("compression", anchor=CENTER, width=120)

        self.im_table.heading("#0", text="", anchor=CENTER)
        self.im_table.heading("name", text="Name", anchor=CENTER)
        self.im_table.heading("size", text="Size", anchor=CENTER)
        self.im_table.heading("dpi", text="DPI", anchor=CENTER)
        self.im_table.heading("depth", text="Color depth", anchor=CENTER)
        self.im_table.heading("compression", text="Compression", anchor=CENTER)

        try:
            count = 0
            for f in os.listdir(self.directory):
                im = Image.open(os.path.join(self.directory, f))
                self.im_table.insert(parent='', index='end', iid = count, text='', 
                    values=(f, im.size, im.info["dpi"], im.mode, im.info["compression"]))
                count += 1
        except OSError as err:
            messagebox.showwarning("Warning", f"OS error: {err}.")

if __name__ == "__main__":
    root = App()
    default_font = tk.font.nametofont("TkDefaultFont")
    default_font.configure(size=12)
    root.geometry("250x250")
    root.option_add("*Font", default_font)
    root.grid_columnconfigure(0, weight=1)
    root.grid_columnconfigure(3, weight=1)
    root.grid_rowconfigure(0, weight=1)
    root.grid_rowconfigure(3, weight=1)
    root.mainloop()
